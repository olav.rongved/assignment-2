# Assignment 2

Vue trivia game for project at noroff

### Extra Functionality

- [x] Added functionality to set number of questions
- [x] Added functionality to set category
- [x] Added functionality to set diffiuclty
- [x] Using Vuex

## Assignment requirements

- [x] Create a new vue application using the CLI
- [x] Use the Vue components to build an application
- [x] Use the VueRouter to navigate between components

### Functionality

**Start Screen**

- [x] App should start on a "Start Screen"
- [x] User must click a button or anywhere on screen to start

**Questions**

- [x] When game starts, fetch questions from API
- [x] Display only ONE question at a time
- [x] If multiple choice, 4 buttons with each answer as the text.
      if it is true/false question, only display 2 buttons, for true and false.
- [x] Once a question is answered the app must move on to the next question
- [x] When all the questions have been answered the user must be pressented with a screen that displays all the questions. This is the result screen.

**Result Screen**

- [x] Result screen must contains the questions with the correctly asnwered questions and display the user score on the result screen

**Scoring**

- [x] Each question should count 10 points. Count all the correctly answered questions and display the users score on the result screen
