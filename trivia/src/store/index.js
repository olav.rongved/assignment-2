import Vuex from "vuex";
import Vue from "vue";
import { fetchQuestionsWithParameters } from "../api/question";
import QuestionModel from "../models/question.js";

Vue.use(Vuex);

//to handle state
const state = {
  questions: [],
  questionsAnswered: 0,
  answers: [],
};

//to handle state
const getters = {
  getAnswerOptions: (state) => (index) => {
    return state.questions[index].allAnswers;
  },
  getQuestion: (state) => (index) => {
    return state.questions[index].question;
  },
  getQuestionsLength: (state) => {
    return state.questions.length;
  },
  getQuestions: (state) => {
    return state.questions;
  },
};

//to handle actions
const actions = {
  incrementQuestionsAnswered({ commit }) {
    commit("incrementQuestions");
  },
  addAnswers({ commit }, answer) {
    commit("addAnswers", answer);
  },
  async createQuestions({ commit }, formData) {
    try {
      await fetchQuestionsWithParameters(
        formData.amount,
        formData.category,
        formData.difficulty
      ).then((response) => {
        response.forEach((o) => {
          let questionModel = new QuestionModel(o);
          questionModel.getAnswers();
          commit("addQuestions", questionModel);
        });
      });
    } catch (error) {
      console.log(error);
    }
  },
};

//to handle mutations
const mutations = {
  incrementQuestions(state) {
    state.questionsAnswered++;
  },
  addAnswers(state, answer) {
    state.answers.push(answer);
  },
  addQuestions(state, question) {
    state.questions.push(question);
  },
  resetState() {
    this.state.questions = [];
    this.state.questionsAnswered = 0;
    this.state.answers = [];
  },
};

//export store module
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
});
