const BASE_URL = "https://opentdb.com/api.php?amount=";

export function fetchQuestionsWithParameters(
  numOfQuestions,
  category,
  difficulty
) {
  let url = generateApiUrl(numOfQuestions, category, difficulty);
  //console.log(url);
  return fetch(`${url}`)
    .then((r) => r.json())
    .then((results) => {
      //console.log(results);
      return results.results;
    });
}

function generateApiUrl(numOfQuestions, category, difficulty) {
  let url = `${BASE_URL}${numOfQuestions}`;
  if (category != "" && category != "any") {
    url += `&category=${category}`;
  }
  //console.log(difficulty);
  if (difficulty != "" && difficulty != "any") {
    url += `&difficulty=${difficulty}`;
  }
  return url;
}
