export default class Question {
  constructor(data) {
    this.question = data.question;
    this.correctAnswer = data.correct_answer;
    this.allAnswers = data.incorrect_answers;
  }

  getAnswers() {
    let answers = this.allAnswers;
    answers.push(this.correctAnswer);
    answers = this.shuffleAnswers(answers);
  }

  shuffleAnswers(answers) {
    answers.sort(() => Math.random() - 0.5);
    return answers;
  }
}
